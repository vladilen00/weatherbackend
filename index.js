/**
 * Использование пакета dotenv для чтения переменных из файла .env в Node.
 */
require('dotenv').config();

const express = require("express");
const rp = require('request-promise');

/**
 * Создание приложения.
 */
const app = express();


/**
 * Обработчик маршрута "/weather".
 */
app.get("/weather", (request, response) => {
	
	response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    response.setHeader('Access-Control-Allow-Credentials', true);
	
	response.setHeader('Access-Control-Allow-Origin', '*');
    
	/**
	 *
	 */
	rp({
		method: 'GET',
		uri: 'https://api.weather.yandex.ru/v1/forecast',
		qs: {
			lat: request.query.lat,
			lon: request.query.lon,
			lang: 'ru_RU'
		},
		headers: {
			'Content-Type' : 'application/json',
			'X-Yandex-API-Key' : process.env.API_KEY
		}	
	})
		.then(result => {
			response.end(result);
		})
		.catch(err => {
			console.log('Ошибка');
			response.status(500).send(err);
		})
});

/**
 * Прослушивание порта 3000.
 */
app.listen(3000);
console.log('Сервер запущен');